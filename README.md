![N|Solid](empresas_react_native/src/Assets/ioasys.png)

# Desafio React Native - ioasys

Este documento `README.md` tem o objetivo de mostrar como o desafio foi implementado.

---
![Image Preview](empresas_react_native/src/Assets/Preview/image_preview.png)
## Objetivo ##

* Desenvolver uma aplicação React Native que consuma a API `Empresas`, cujo Postman esta compartilhado neste repositório (collection).

## Criação do Design ##
* Inicialmente, analisando os dados da API, fiz um pequeno planejamento e desenvolvi o design no Figma: https://www.figma.com/file/vHtB7UAsac2Kbf07oWyF5l/Ioasys-Test?node-id=0%3A1

## Preview ##
![Video Preview](empresas_react_native/src/Assets/Preview/appVideo.gif)


### O download do vídeo completo da aplicação rodando (appVideo.mov) pode ser encontrado neste link: https://bitbucket.org/joorge_roberto/empresas-react-native/src/master/empresas_react_native/src/Assets/Preview/



---


## Como executar a aplicação: ##
* OBS: Para executar esta aplicação você deve ter o React Native configurado:
	* Para configurar, siga o passo-a-passo selecionando a opção `React Native CLI Quickstart` neste link: https://reactnative.dev/docs/environment-setup


### Passo 1: Clonar o repositório: ###

```
$ git clone https://bitbucket.org/joorge_roberto/empresas-react-native.git

$ cd empresas-react-native/empresas_react_native
```

### Passo 2: Instalar depedências ###

```
$ yarn
```

_ou_

```
$ npm install
```
### Passo 3: Rodar o projeto ###
```
$ react-native run-ios
```
_ou_

```
$ react-native run-android
```
**Qualquer dificuldade na instalação do React Native ou na execução do projeto, estou a disposição nos emails: joorge_roberto@hotmail.com e joorgercj@dcomp.ufs.br**

**O .apk para instalar a aplicação em dispositivos Android pode ser baixado neste link: https://bitbucket.org/joorge_roberto/empresas-react-native/src/master/empresas_react_native/apk/**

## Dados para Teste ##
* Servidor: http://empresas.ioasys.com.br
* Versão 
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

## Justificativa para bibliotecas adicionadas: ##
* Como sugerido, tentei ao máximo usar componentes criados por mim e evitar o uso de bibliotecas de terceiros. Logo, as bibliotecas utilizadas foram apenas as que julguei essenciais para o desenvolvimento.
* Utilizei:
	* `Axios` e `Apisauce` para fazer facilitar a integração com a API fornecida.
	* `react-navigation V4`para fazer a navegação entre as telas utilizando o Switch Navigator.
	* `react-redux` e `redux` para pode utilizar o redux.
	* `redux-thunk` como middleware. Poderia ter utilizado o redux-sagas mas preferi usar o redux-thunk por este ser mais simples e o projeto ser pequeno.
	* `react-native-gesture-handler` é utilizado internamente pela `react-navigation` por isso também foi instalada para evitar erros.

## Sobre os endpoints:
* Como foi pedido, utilizei os todos os endpoints disponibilizados. Tenho ciência que o endpoint de `Detalhamento de Empresas: /enterprises/{id}` poderia não ser utilizado e aproveitar os dados da empresa que já foram requisitados na tela anterior (Listagem de Empresas). 
* Caso a lista de empresas cresça, seria interessante adicionar paginação no endpoint de `Listagem de Empresas: /enterprises` com isso, o carregamento poderia ser ainda rápido.

## O eu que poderia adicionar para melhorar este projeto futuramente: 
* Adicionar Typescript.
* Criar um Custom Hook (useAnimation) para gerenciar as animações.
* Também acredito que poderia ter utilizado styled-components para facilitar o reuso dos componentes criados.
* Adicionar Testes unitários, interface, etc.


## Aceito sugestões para melhorar o projeto :)




## Agradeço a atenção e a oportunidade. Desejo paz e sucesso a todos :)
