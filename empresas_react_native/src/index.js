import React from 'react';
import {
  StatusBar,
  SafeAreaView,
  LogBox,
} from 'react-native';
import Routes from './Config/Routes';
import reducers from './Reducers';
import {Provider} from 'react-redux';
import ReduxThunk from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';

LogBox.ignoreLogs([
  'Warning',
]);

const App = () => {
  const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
  return (
    <>
      <Provider store={store}>
        <StatusBar barStyle="dark-content" backgroundColor={'white'} />
        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
          <Routes />
        </SafeAreaView>
      </Provider>
    </>
  );
};

export default App;
