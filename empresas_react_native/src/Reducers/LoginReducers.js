import {
  LOGIN,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  UPDATE_LOGIN_DATA,
} from '../Actions/types.js';

const INITIAL_STATE = {
  access_token: null,
  uid: null,
  client: null,
  investor: null,
  enterprise: null,
  loading: false,
  error: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN: {
      return {
        ...state,
        error: '',
        loading: true,
      };
    }
    case LOGIN_SUCCESS: {
      return {
        ...state,
        error: '',
        loading: false,
        access_token: action.payload.access_token,
        uid: action.payload.uid,
        client: action.payload.client,
        investor: action.payload.investor,
        enterprise: action.payload.enterprise,
      };
    }
    case LOGIN_FAIL: {
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    }
    case UPDATE_LOGIN_DATA: {
      return {...state, [action.payload.prop]: action.payload.value};
    }
    default:
      return state;
  }
};
