import {combineReducers} from 'redux';
import HomeReducers from './HomeReducers';
import LoginReducers from './LoginReducers';
import DetailsReducers from './DetailsReducers';

export default combineReducers({
  home: HomeReducers,
  login: LoginReducers,
  details: DetailsReducers,
});
