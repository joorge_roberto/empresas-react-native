import {
  GET_DETAILS,
  GET_DETAILS_FAIL,
  GET_DETAILS_SUCCESS,
  UPDATE_DETAILS_DATA,
} from '../Actions/types.js';

const INITIAL_STATE = {
  details: null,
  loading: false,
  error: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_DETAILS: {
      return {
        ...state,
        error: '',
        loading: true,
      };
    }
    case GET_DETAILS_SUCCESS: {
      return {
        ...state,
        error: '',
        loading: false,
        details: action.payload,
      };
    }
    case GET_DETAILS_FAIL: {
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    }
    case UPDATE_DETAILS_DATA: {
      return {...state, [action.payload.prop]: action.payload.value};
    }
    default:
      return state;
  }
};
