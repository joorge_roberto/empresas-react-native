import {
  GET_ENTERPRISES,
  GET_ENTERPRISES_FAIL,
  GET_ENTERPRISES_SUCCESS,
  SAVE_ENTERPRISES_TYPES,
  UPDATE_HOME_DATA,
} from '../Actions/types.js';

const INITIAL_STATE = {
  enterprises: null,
  loading: false,
  enterprises_type: null,
  searchInputValue: '',
  selectedEnterpriseType: 'All',
  error: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_ENTERPRISES: {
      return {
        ...state,
        error: '',
        loading: true,
      };
    }
    case GET_ENTERPRISES_SUCCESS: {
      return {
        ...state,
        error: '',
        loading: false,
        enterprises: action.payload,
      };
    }
    case GET_ENTERPRISES_FAIL: {
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    }
    case SAVE_ENTERPRISES_TYPES: {
      return {
        ...state,
        enterprises_type: action.payload,
      };
    }
    case UPDATE_HOME_DATA: {
      return {...state, [action.payload.prop]: action.payload.value};
    }
    default:
      return state;
  }
};
