import React, {useEffect, useRef, useState} from 'react';
import {
  Platform,
  KeyboardAvoidingView,
  FlatList,
  Animated,
} from 'react-native';
import {
  AnimatedLogo,
  AnimatedView,
  EnterpriseItemList,
  Loader,
  SearchBar,
} from '../../Components';
import {styles} from './styles';

import {useDispatch, useSelector} from 'react-redux';
import {getEnterprises} from '../../Actions';
import {fadeIn} from '../../Config/Animations';

const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const animation = useRef(new Animated.Value(0)).current;
  const [hasAnimation, setHasAnimation] = useState(true);
  const {loading, enterprises} = useSelector((state) => state.home);
  const {access_token, client, uid} = useSelector((state) => state.login);

  useEffect(() => {
    if (enterprises === null) {
      setHasAnimation(true);
      fadeIn(animation);
      dispatch(getEnterprises({access_token, client, uid}));
    } else {
      setHasAnimation(false);
    }
  }, []);
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <AnimatedLogo hasAnimation={hasAnimation} small animation={animation} />
      <AnimatedView hasAnimation={hasAnimation} animation={animation}>
        <SearchBar />
        {loading || enterprises === null ? (
          <Loader size={100} color={'black'} />
        ) : (
          <>
            <FlatList
              numColumns={2}
              style={styles.enterpriseListStyle}
              data={enterprises}
              horizontal={false}
              renderItem={({item}) => (
                <EnterpriseItemList
                  onPress={() => navigation.navigate('Details', {data: item})}
                  data={item}
                />
              )}
              keyExtractor={(item) => item}
            />
          </>
        )}
      </AnimatedView>
    </KeyboardAvoidingView>
  );
};

export {Home};
