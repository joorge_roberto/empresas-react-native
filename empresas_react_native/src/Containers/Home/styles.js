import {StyleSheet} from 'react-native';
import {Dimensions} from 'react-native';
import Colors from '../../Config/Colors';
const {width} = Dimensions.get('window');

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    paddingTop: 20,
    paddingBottom: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  enterpriseListStyle: {
    width: width - 16,
    flex: 1,
  },
});
