import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    marginBottom: 40,
    padding: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  inputsContainer: {
    width: '100%',
  },
});
