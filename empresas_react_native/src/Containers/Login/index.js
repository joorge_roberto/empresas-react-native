import React, {useRef, useState, useEffect} from 'react';
import {
  View,
  Animated,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  Alert,
} from 'react-native';
import {styles} from './styles';
import {Button, TextInput, AnimatedLogo, AnimatedView} from '../../Components';
import {fadeIn} from '../../Config/Animations';

import {login, updateLoginData} from '../../Actions';
import {useDispatch, useSelector} from 'react-redux';

const Login = ({navigation}) => {
  const dispatch = useDispatch();
  const {error, loading} = useSelector((state) => state.login);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const animation = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    if (error !== '') {
      Alert.alert('Error', error);
      dispatch(updateLoginData({prop: 'error', value: ''}));
    }
  }, [error]);

  useEffect(() => {
    fadeIn(animation);
  }, [])
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <AnimatedLogo hasAnimation animation={animation} />
      <AnimatedView hasAnimation login animation={animation}>
        <View style={styles.inputsContainer}>
          <TextInput
            label={'Email:'}
            placeholder={'email@ioasys.com'}
            value={email}
            onChangeText={(email) => {
              setEmail(email);
            }}
            keyboardType={'email-address'}
            returnKeyType={'next'}
          />
          <TextInput
            label={'Password:'}
            secureTextEntry
            placeholder={'********'}
            value={password}
            onChangeText={(password) => {
              setPassword(password);
            }}
          />
        </View>
        <Button
          loading={loading}
          onPress={() => {
            Keyboard.dismiss();
            dispatch(login({email, password, navigation, animation}));
          }}
          text={'LOGIN'}
        />
      </AnimatedView>
    </KeyboardAvoidingView>
  );
};

export {Login};
