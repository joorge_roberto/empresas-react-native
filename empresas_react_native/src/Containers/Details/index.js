import React, {useEffect} from 'react';
import {View, Image, ScrollView} from 'react-native';
import {
  Header,
  Loader,
  DetailsTextInfo,
  TypeAndSharePrice,
} from '../../Components';
import {styles} from './styles';

import {useDispatch, useSelector} from 'react-redux';
import {getDetails, updateDetails} from '../../Actions';

const Details = ({navigation}) => {
  const dispatch = useDispatch();
  const {loading, details} = useSelector((state) => state.details);
  const {access_token, client, uid} = useSelector((state) => state.login);
  const {enterprise_name, id} = navigation.state.params.data;

  useEffect(() => {
    dispatch(getDetails({access_token, client, uid, enterpriseId: id}));
  }, []);

  return (
    <View style={styles.container}>
      <Header
        iconOnPress={() => {
          navigation.navigate('Home');
          dispatch(updateDetails({prop: 'details', value: null}));
        }}
        title={enterprise_name}
      />
      {!loading && details ? (
        <ScrollView style={styles.scrollViewContainer}>
          <Image
            source={{uri: `https://empresas.ioasys.com.br${details.photo}`}}
            style={styles.imageStyle}
          />
          <DetailsTextInfo
            title={'Location:'}
            info={`${details.country}, ${details.city}`}
          />
          <TypeAndSharePrice
            type={details.enterprise_type.enterprise_type_name}
            sharePrice={details.share_price}
          />
          <DetailsTextInfo
            left
            title={'Description:'}
            info={`${details.description}`}
          />
        </ScrollView>
      ) : (
        <Loader size={100} color={'black'} />
      )}
    </View>
  );
};

export {Details};
