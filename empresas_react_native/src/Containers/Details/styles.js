import {StyleSheet} from 'react-native';
import Colors from '../../Config/Colors';
import {calculateVwValue} from '../../Config/Functions';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    paddingLeft: 16,
    paddingRight: 16,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  scrollViewContainer: {
    flex: 1,
    width: '100%',
  },
  imageStyle: {
    width: '100%',
    height: calculateVwValue(189.09),
    borderRadius: 10,
    backgroundColor: Colors.backgroundLightGrey,
  },
});
