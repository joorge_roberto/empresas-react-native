import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import {Login, Home, Details} from '../Containers';

const Routes = createAppContainer(
  createSwitchNavigator(
    {
      Login,
      Home,
      Details,
    },
    {
      initialRouteName: 'Login',
      unmountInactiveRoutes: true,
    },
  ),
);

export default Routes;
