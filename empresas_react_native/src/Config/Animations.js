import {Animated} from 'react-native';

export const fadeIn = (anim, duration) => {
  Animated.timing(anim, {
    toValue: 1,
    duration: duration ? duration : 1000,
    useNativeDriver: true,
  }).start();
};

export const fadeOut = (anim) => {
  Animated.timing(anim, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  }).start();
};

export const translateY = (anim) =>
  anim.interpolate({
    inputRange: [0, 1],
    outputRange: [300, 0],
  });

export const scale = (anim) =>
  anim.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1],
  });
