const Colors = {
  primary: '#000000',
  white: '#FFFFFF',
  backgroundLightGrey: 'rgba(0, 0, 0, 0.05)',
  backgroundMediumGrey: 'rgba(0, 0, 0, 0.4)',
  textLightGrey: 'rgba(0, 0, 0, 0.3)',
  textMediumGrey: 'rgba(0, 0, 0, 0.5)',
  labelGrey: 'rgba(0, 0, 0, 0.7)',
};

export default Colors;
