import {create} from 'apisauce';

const BASE_URL = 'https://empresas.ioasys.com.br/api/v1/';

const Api = create({
  baseURL: `${BASE_URL}`,

  timeout: 30000,
});

export default Api;
