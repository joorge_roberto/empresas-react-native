import {Dimensions} from 'react-native';
const {width} = Dimensions.get('window');

export function calculateVwValue(value) {
  return width * (value / 375);
}
