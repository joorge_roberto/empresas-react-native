import {
  GET_DETAILS,
  GET_DETAILS_SUCCESS,
  GET_DETAILS_FAIL,
  UPDATE_DETAILS_DATA,
} from './types.js';

import Api from '../Config/Api';

export const updateDetails = ({prop, value}) => {
  return {
    type: UPDATE_DETAILS_DATA,
    payload: {prop, value},
  };
};

export const getDetails = ({access_token, client, uid, enterpriseId}) => {
  return(dispatch) => {
    dispatch({type: GET_DETAILS});
    async function loadData() {
      await Api.get(
        `enterprises/${enterpriseId}`,
        {},
        {
          headers: {
            'Content-Type': 'application/json',
            'access-token': access_token,
            client: client,
            uid: uid,
          },
        },
      )
        .then((res) => {
          dispatch({
            type: GET_DETAILS_SUCCESS,
            payload: res.data.enterprise,
          });
        })
        .catch((error) => {
          dispatch({
            type: GET_DETAILS_FAIL,
            payload: error,
          });
        });
    }

    loadData();
  }
};
