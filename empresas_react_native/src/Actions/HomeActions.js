import {
  GET_ENTERPRISES,
  GET_ENTERPRISES_SUCCESS,
  GET_ENTERPRISES_FAIL,
  SAVE_ENTERPRISES_TYPES,
  UPDATE_HOME_DATA,
} from './types.js';

import Api from '../Config/Api';

export const updateHomeData = ({prop, value}) => {
  return {
    type: UPDATE_HOME_DATA,
    payload: {prop, value},
  };
};

export const getEnterprises = ({
  access_token,
  client,
  uid,
  type,
  name = '',
}) => {
  return(dispatch) => {
    dispatch({type: GET_ENTERPRISES});

    let params = {
      enterprise_types: null,
      name: null,
    };
    if (
      type &&
      type.enterprise_type_name &&
      type.enterprise_type_name !== 'All'
    ) {
      params.enterprise_types = type.id;
    }
    if (name !== '') {
      params.name = name;
    }
    const isFirstRequest =
      params.enterprise_types === null && params.name === null;

    async function loadData() {
      await Api.get('enterprises', params, {
        headers: {
          'Content-Type': 'application/json',
          'access-token': access_token,
          client: client,
          uid: uid,
        },
      })
        .then((res) => {
          const enterprises = res.data.enterprises;
          dispatch({
            type: GET_ENTERPRISES_SUCCESS,
            payload: enterprises,
          });
          if (isFirstRequest) {
            let enterprises_type =
              enterprises &&
              enterprises.map((enterprise) => enterprise.enterprise_type);
            enterprises_type = enterprises_type.filter(
              (arr, index, self) =>
                index ===
                self.findIndex(
                  (t) =>
                    t.id === arr.id &&
                    t.enterprise_type_name === arr.enterprise_type_name,
                ),
            );
            enterprises_type = [{enterprise_type_name: 'All'}].concat(
              enterprises_type,
            );
            //console.log('enterprises_type', enterprises_type)
            dispatch({
              type: SAVE_ENTERPRISES_TYPES,
              payload: enterprises_type,
            });
          }
        })
        .catch((error) => {
          dispatch({
            type: GET_ENTERPRISES_FAIL,
            payload: error,
          });
        });
    }
    loadData();
  };
};
