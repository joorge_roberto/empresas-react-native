import {LOGIN, LOGIN_SUCCESS, LOGIN_FAIL, UPDATE_LOGIN_DATA} from './types.js';

import Api from '../Config/Api';
import {fadeOut} from '../Config/Animations';

export const updateLoginData = ({prop, value}) => {
  return {
    type: UPDATE_LOGIN_DATA,
    payload: {prop, value},
  };
};

export const login = ({email, password, navigation, animation}) => {
  //const mock_email = 'testeapple@ioasys.com.br';
  //const mock_password = '12341234';
  const isInputValid =
    email !== '' &&
    email.match(/^[\w.-]+@[\w-]+\.[a-z]{2,}(\.[a-z]{2,})?$/) &&
    password.length > 3;
  return (dispatch) => {
    dispatch({type: LOGIN});

    async function loadData() {
      await Api.post(
        'users/auth/sign_in',
        {
          email,
          password,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      )
        .then((res) => {
          if (res.data.success) {
            dispatch({
              type: LOGIN_SUCCESS,
              payload: {
                access_token: res.headers['access-token'],
                uid: res.headers.uid,
                client: res.headers.client,
                investor: res.data.investor,
                enterprise: res.data.enterprise,
              },
            });
            fadeOut(animation);
            setTimeout(() => navigation.navigate('Home'), 1200);
          } else {
            if (res.data.errors && res.data.errors.length > 0)
              dispatch({
                type: LOGIN_FAIL,
                payload: res.data.errors[0],
              });
          }
        })
        .catch((error) => {
          console.error(error);
          dispatch({
            type: LOGIN_FAIL,
            payload: 'Please, try again.',
          });
        });
    }
    if (isInputValid) loadData();
    else {
      dispatch({
        type: LOGIN_FAIL,
        payload: 'Invalid login credentials. Please try again.',
      });
    }
  };
};
