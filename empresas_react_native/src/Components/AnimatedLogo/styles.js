import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: 220,
    height: 103,
  },
  smallImageContainer: {
    width: 149,
    height: 72,
    marginBottom: 26,
  },
  imageStyle: {
    width: '100%',
    height: '100%',
  },
});
