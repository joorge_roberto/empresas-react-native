import React from 'react';
import {Animated, Image} from 'react-native';
import {styles} from './styles';
import {translateY} from '../../Config/Animations';

const AnimatedLogo = ({
  animation,
  hasAnimation = false,
  small = false,
  style,
}) => {
  return (
    <Animated.View
      style={[
        small ? styles.smallImageContainer : styles.container,
        [
          hasAnimation
            ? {
                transform: [{translateY: translateY(animation)}],
              }
            : {},
        ],
        style,
      ]}>
      <Image
        source={require('../../Assets/ioasys.png')}
        style={styles.imageStyle}
      />
    </Animated.View>
  );
};

export {AnimatedLogo};
