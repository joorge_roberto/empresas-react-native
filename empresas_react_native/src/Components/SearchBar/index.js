import React, {useState} from 'react';
import {View} from 'react-native';
import {styles} from './styles';
import {OptionsModal, SearchInput} from '../../Components';
import {useDispatch, useSelector} from 'react-redux';
import {getEnterprises, updateHomeData} from '../../Actions';

const SearchBar = ({style}) => {
  const [visible, setVisible] = useState(false);
  const dispatch = useDispatch();
  const {access_token, client, uid} = useSelector((state) => state.login);
  const {
    enterprises_type,
    searchInputValue,
    selectedEnterpriseType,
  } = useSelector((state) => state.home);

  function returnSelectedOption(option) {
    let filtered = enterprises_type.filter(
      (type) => type.enterprise_type_name === option,
    );
    return filtered.length > 0 ? filtered[0] : {};
  }

  const returnSubmitDispatch = (option) =>
    dispatch(
      getEnterprises({
        access_token,
        client,
        uid,
        type: option ? option : returnSelectedOption(selectedEnterpriseType),
        name: searchInputValue,
      }),
    );

  function optionsList() {
    let options =
      enterprises_type &&
      enterprises_type.map((enterprise) => enterprise.enterprise_type_name);
    return options;
  }

  return (
    <View style={[styles.container, style]}>
      <SearchInput
        value={searchInputValue}
        onChangeText={(text) => {
          dispatch(updateHomeData({prop: 'searchInputValue', value: text}));
        }}
        onSubmitEditing={() => returnSubmitDispatch()}
        onBlur={() => returnSubmitDispatch()}
      />
      <OptionsModal
        selectedOption={selectedEnterpriseType}
        inputText={selectedEnterpriseType}
        setSelectedOption={(option) => {
          dispatch(
            updateHomeData({
              prop: 'selectedEnterpriseType',
              value: option,
            }),
          );
          returnSubmitDispatch(returnSelectedOption(option));
        }}
        options={optionsList()}
        isVisible={visible}
        setIsVisible={(value) => setVisible(value)}
      />
    </View>
  );
};

export {SearchBar};
