import {StyleSheet} from 'react-native';
import Colors from '../../Config/Colors';

export const styles = StyleSheet.create({
  buttonStyle: {
    width: 254,
    height: 48,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: Colors.primary,
    zIndex: 1,
  },
  textStyle: {
    fontSize: 14,
    color: 'white',
    fontWeight: 'bold',
  },
});
