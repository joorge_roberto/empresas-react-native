import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {Loader} from '../Loader';
import {styles} from './styles';
import Colors from '../../Config/Colors';

const Button = ({
  onPress,
  text,
  style,
  loading = false,
  sympleStyle = false,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={loading ? null : onPress}
      style={[
        styles.buttonStyle,
        {backgroundColor: sympleStyle ? Colors.white : Colors.primary},
        style,
      ]}>
      {loading ? (
        <Loader size={20} />
      ) : (
        <Text
          style={[
            styles.textStyle,
            {color: sympleStyle ? Colors.primary : Colors.white},
          ]}>
          {text}
        </Text>
      )}
    </TouchableOpacity>
  );
};

export {Button};
