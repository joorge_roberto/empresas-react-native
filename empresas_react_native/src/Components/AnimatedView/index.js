import React from 'react';
import {Animated} from 'react-native';
import {styles} from './styles';
import {scale} from '../../Config/Animations';

const AnimatedView = ({
  animation,
  children,
  login = false,
  hasAnimation = false,
}) => {
  return (
    <Animated.View
      style={[
        styles.container,
        login ? styles.loginContainer : null,
        [
          hasAnimation
            ? {
                transform: [{scale: scale(animation)}],
                opacity: animation,
              }
            : {},
        ],
      ]}>
      {children}
    </Animated.View>
  );
};

export {AnimatedView};
