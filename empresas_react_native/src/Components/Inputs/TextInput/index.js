import React from 'react';
import {View, Text, TextInput as RawInput} from 'react-native';
import {styles} from './styles';

const TextInput = ({
  label,
  value,
  onChangeText,
  style,
  placeholder = 'Insert text',
  keyboardType = 'default',
  secureTextEntry,
  returnKeyType,
  autoCapitalize = 'none',
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.labelStyle}>{label}</Text>
      <RawInput
        onChangeText={(text) => onChangeText(text)}
        value={value}
        placeholderTextColor={'#979797'}
        placeholder={placeholder}
        autoCapitalize={autoCapitalize}
        style={[styles.textFieldStyle, {style}]}
        keyboardType={keyboardType}
        returnKeyType={returnKeyType}
        secureTextEntry={secureTextEntry}
      />
    </View>
  );
};

export {TextInput};
