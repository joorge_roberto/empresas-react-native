import {StyleSheet} from 'react-native';
import Colors from '../../../Config/Colors';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginBottom: 21,
  },
  textFieldStyle: {
    fontSize: 16,
    color: Colors.primary,
    marginTop: 12,
    paddingBottom: 3,
    paddingLeft: 6,
    borderBottomWidth: 1,
    borderBottomColor: Colors.textLightGrey,
  },
  labelStyle: {
    fontSize: 16,
    color: Colors.labelGrey,
  }
});
