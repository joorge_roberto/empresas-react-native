import {StyleSheet} from 'react-native';
import Colors from '../../../Config/Colors';
import {calculateVwValue} from '../../../Config/Functions';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginRight: 7,
    flexDirection: 'row',
    height: calculateVwValue(40),
    backgroundColor: Colors.backgroundLightGrey,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
    paddingLeft: 10,
  },
  textFieldStyle: {
    fontSize: 18,
    flex: 1,
    marginLeft: 9,
    color: Colors.labelGrey,
  },
});
