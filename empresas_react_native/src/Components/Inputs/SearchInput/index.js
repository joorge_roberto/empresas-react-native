import React from 'react';
import {View, TextInput, Image} from 'react-native';
import {styles} from './styles';
import Colors from '../../../Config/Colors';

const SearchInput = ({
  value,
  onChangeText,
  style,
  placeholder = 'Search',
  keyboardType = 'default',
  secureTextEntry,
  returnKeyType,
  autoCapitalize,
  onSubmitEditing,
  onBlur,
}) => {
  return (
    <View style={styles.container}>
      <Image source={require('../../../Assets/search.png')} />
      <TextInput
        onChangeText={(text) => onChangeText(text)}
        value={value}
        placeholderTextColor={Colors.labelGrey}
        placeholder={placeholder}
        autoCapitalize={autoCapitalize}
        style={[styles.textFieldStyle, {style}]}
        keyboardType={keyboardType}
        returnKeyType={returnKeyType}
        secureTextEntry={secureTextEntry}
        onSubmitEditing={onSubmitEditing}
        onBlur={onBlur}
      />
    </View>
  );
};

export {SearchInput};
