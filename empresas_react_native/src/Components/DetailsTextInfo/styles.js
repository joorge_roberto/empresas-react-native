import {StyleSheet} from 'react-native';
import Colors from '../../Config/Colors';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 11,
  },
  titleTextStyle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: Colors.primary,
  },
  infoTextStyle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: Colors.textMediumGrey,
    marginTop: 3,
  },
  descriptionContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 20,
  },
});
