import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './styles';

const DetailsTextInfo = ({title, info, left = false}) => {
  return (
    <View style={left ? styles.descriptionContainer : styles.container}>
      <Text style={styles.titleTextStyle}>{title}</Text>
      <Text style={styles.infoTextStyle}>{info}</Text>
    </View>
  );
};

export {DetailsTextInfo};
