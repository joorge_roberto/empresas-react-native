import {StyleSheet} from 'react-native';
import Colors from '../../Config/Colors';

export const styles = StyleSheet.create({
  container: {
    paddingTop: 9,
    paddingBottom: 9,
    paddingLeft: 18,
    paddingRight: 18,
    backgroundColor: Colors.backgroundLightGrey,
    borderRadius: 10,
    marginTop: 5,
  },
  textStyle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: Colors.labelGrey,
    textAlign: 'center',
  },
  sharePriceTextStyle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: Colors.white,
    textAlign: 'center',
  },
  sharePriceContainer: {
    paddingTop: 9,
    paddingBottom: 9,
    paddingLeft: 18,
    paddingRight: 18,
    backgroundColor: Colors.backgroundMediumGrey,
    borderRadius: 10,
    marginTop: 5,
  },
});
