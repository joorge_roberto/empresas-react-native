import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './styles';

const TextWithRoundedBackground = ({sharePrice = false, text, style}) => {
  return !sharePrice ? (
    <View style={[styles.container, style]}>
      <Text style={styles.textStyle}>{text}</Text>
    </View>
  ) : (
    <View style={[styles.sharePriceContainer, style]}>
      <Text style={styles.sharePriceTextStyle}>U$ {text}</Text>
    </View>
  );
};

export {TextWithRoundedBackground};
