import React from 'react';
import {View, ActivityIndicator as Spinner} from 'react-native';
import {styles} from './styles';

const Loader = ({size, color, marginTop}) => {
  return (
    <View style={[styles.container, {marginTop: marginTop ? 100 : 0}]}>
      <Spinner color={color ? color : 'white'} size={size ? size : 100} />
    </View>
  );
};

export {Loader};
