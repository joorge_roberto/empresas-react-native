import React from 'react';
import {View, Text} from 'react-native';
import {TextWithRoundedBackground} from '../../Components';
import {styles} from './styles';

const TypeAndSharePrice = ({type, sharePrice}) => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.typeTitleStyle}>Type:</Text>
        <TextWithRoundedBackground text={type} />
      </View>
      <View style={styles.sharePriceContainer}>
        <Text style={styles.typeTitleStyle}>Share Price:</Text>
        <TextWithRoundedBackground
          sharePrice
          text={sharePrice}
        />
      </View>
    </View>
  );
};

export {TypeAndSharePrice};
