import {StyleSheet} from 'react-native';
import Colors from '../../Config/Colors';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 14,
  },
  typeTitleStyle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: Colors.primary,
    textAlign: 'center',
  },
  sharePriceContainer: {
    backgroundColor: Colors.backgroundLightGrey,
    padding: 10,
    borderRadius: 10,
  },
});
