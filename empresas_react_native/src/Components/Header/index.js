import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {styles} from './styles';

const Header = ({iconOnPress, title}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.imageContainer}
        onPress={iconOnPress}
        activeOpacity={0.9}>
        <Image source={require('../../Assets/arrow-back.png')} />
      </TouchableOpacity>
      <Text style={styles.titleTextStyle}>{title}</Text>
    </View>
  );
};

export {Header};
