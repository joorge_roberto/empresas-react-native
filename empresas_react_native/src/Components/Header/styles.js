import {StyleSheet} from 'react-native';
import Colors from '../../Config/Colors';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    minHeight: 40,
    marginBottom: 21,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  imageContainer: {
    width: 16,
    marginRight: 16,
  },
  titleTextStyle: {
    fontSize: 22,
    paddingRight: 32,
    textAlign: 'center',
    flex: 1,
    fontWeight: 'bold',
    color: Colors.primary,
  },
});
