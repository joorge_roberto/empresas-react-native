import {StyleSheet} from 'react-native';
import Colors from '../../Config/Colors';
import {calculateVwValue} from '../../Config/Functions';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 16,
    maxWidth: 200,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.backgroundLightGrey,
    borderRadius: 10,
  },
  imageStyle: {
    width: '100%',
    height: calculateVwValue(105),
    borderRadius: 10,
    backgroundColor: Colors.textLightGrey,
  },
  enterpriseNameStyle: {
    marginTop: 8,
    color: Colors.primary,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    maxWidth: '90%',
  },
  locationStyle: {
    marginTop: 4,
    color: Colors.textMediumGrey,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  enterpriseTypeStyle: {
    marginTop: 4,
    color: Colors.primary,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  sharePriceContainer: {
    backgroundColor: Colors.backgroundMediumGrey,
    width: '82%',
    paddingBottom: 8,
    paddingTop: 8,
    marginTop: 10,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sharePriceStyle: {
    color: Colors.white,
    fontSize: 14,
    fontWeight: 'bold',
  },
});
