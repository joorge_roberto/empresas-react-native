import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {styles} from './styles';

const EnterpriseItemList = ({data, style, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.9}
      style={[styles.container, style]}>
      <Image
        source={{uri: `https://empresas.ioasys.com.br${data.photo}`}}
        style={styles.imageStyle}
      />
      <Text style={styles.enterpriseNameStyle}>{data.enterprise_name}</Text>
      <Text style={styles.locationStyle}>
        {data.country}, {data.city}
      </Text>
      <View style={styles.sharePriceContainer}>
        <Text style={styles.sharePriceStyle}>U$ {data.share_price}</Text>
      </View>
      <Text style={styles.enterpriseTypeStyle}>
        {data.enterprise_type.enterprise_type_name}
      </Text>
    </TouchableOpacity>
  );
};

export {EnterpriseItemList};
