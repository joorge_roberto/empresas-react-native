import React, {useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  Modal,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';
import Colors from '../../Config/Colors';
import {styles} from './styles';
import {Button} from '../Button';

const {height} = Dimensions.get('window');

const OptionsModal = ({
  isVisible,
  setIsVisible,
  inputText,
  modalTitle = 'Select an Enterprise Type:',
  options = ['All', 'Health', 'Service', 'IT', 'Fintech', 'Social'],
  selectedOption,
  setSelectedOption,
}) => {
  const [innerOption, setInnerOption] = useState(selectedOption);

  const touchableItem = (item) => {
    return (
      <TouchableOpacity
        onPress={() => setInnerOption(item)}
        style={[
          styles.optionContainerStyle,
          {
            backgroundColor:
              item === innerOption
                ? Colors.primary
                : 'transparent',
          },
        ]}
        activeOpacity={0.7}>
        <Text
          style={[
            styles.optionTextStyle,
            {
              color: item === innerOption ? 'white' : 'black',
            },
          ]}>
          {item}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => setIsVisible(true)}
        style={styles.buttonContainer}>
        <Text
          numberOfLines={1}
          ellipsizeMode={'tail'}
          style={styles.buttonTextStyle}>
          {inputText !== '' ? inputText : 'Type'}
        </Text>
        <Image source={require('../../Assets/angle-down.png')} />
      </TouchableOpacity>
      <Modal
        visible={isVisible}
        transparent={true}
        animationType={'slide'}
        onRequestClose={() => {}}
        style={styles.modalContainer}>
        <View style={styles.container}>
          <View style={styles.cardStyle}>
            <Text style={styles.titleTextStyle}>{modalTitle}</Text>
            <FlatList
              style={{width: '100%', maxHeight: height * 0.3}}
              data={options}
              renderItem={({item}) => touchableItem(item)}
              keyExtractor={(item) => item}
            />
            <View style={styles.navigationButtonsContainer}>
              <Button
                sympleStyle
                text={'CLOSE'}
                style={{width: '40%'}}
                onPress={() => setIsVisible(false)}
              />
              <Button
                text={'CONFIRM'}
                onPress={() => {
                  setIsVisible(false);
                  innerOption && innerOption !== selectedOption
                    ? setSelectedOption(innerOption)
                    : null;
                }}
                style={{width: '50%'}}
              />
            </View>
          </View>
        </View>
      </Modal>
    </>
  );
};

export {OptionsModal};
