import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {
  Dimensions,
} from 'react-native';
import Colors from '../../Config/Colors';
import {calculateVwValue} from '../../Config/Functions';
import React from 'react';
const {width, height} = Dimensions.get('window');

export const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.textLightGrey,
  },
  titleTextStyle: {
    fontSize: 21.3,
    fontWeight: 'bold',
    color: Colors.primary,
    marginBottom: 29,
    textAlign: 'center',
    width: '100%',
  },
  cardStyle: {
    width: width - calculateVwValue(40),
    backgroundColor: 'white',
    padding: calculateVwValue(20),
    borderRadius: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  buttonContainer: {
    height: calculateVwValue(40),
    width: calculateVwValue(85),
    maxWidth: 85,
    borderColor: Colors.textMediumGrey,
    borderWidth: 1,
    borderRadius: 10,
    paddingLeft: calculateVwValue(8),
    paddingRight: calculateVwValue(8),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonTextStyle: {
    color: Colors.textMediumGrey,
    fontSize: 16,
    maxWidth: calculateVwValue(50),
  },
  navigationButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    paddingLeft: width * 0.0884,
    paddingRight: width * 0.0884,
    marginTop: 40,
    height: 50,
  },
  optionContainerStyle: {
    backgroundColor: 'white',
    width: '100%',
    paddingBottom: 10,
    paddingTop: 10,
    borderBottomColor: Colors.primary,
  },
  optionTextStyle: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 'bold',
  },
});
